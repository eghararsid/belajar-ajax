<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserResource as UserRes;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $user = User::orderBy('created_at', 'desc')->get();

        return UserRes::collection($user);
    }

    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
        ]);

        return new UserRes($user);
    }

    public function delete($id)
    {
        User::destroy($id);
        return 'success';
    }

    public function changeName(Request $request, $id)
    {
        $user = User::find($id);
        
        $user->update([
            'name' => $request->name
        ]);

        return new UserRes($user);
    }
}
