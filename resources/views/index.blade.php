<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Belajar AJAX</title>
</head>
<body>
    <div id="app">
		<input type="text" v-if="show == 'Add'" v-model="nama" @keyup.enter="newPerson">
        <input type="text" v-else="show" v-model="nama" @keyup.enter="updatePerson">
		<button v-if="show == 'Add'" @click='newPerson' v-text="button1"></button>
		<button v-else="show" @click='updatePerson' v-text="button3"></button><br>
		<ul>
			<li v-for="(user, index) in users">
				<span>@{{ user.name }}</span>
				<button v-text="button2" @click="changeButton(user, index)"></button>
				<span> | </span>
				<button v-text="button4" @click='deletePerson(user, index)'></button><br><br>
			</li>
		</ul>
	</div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                nama: '',
                button1: 'Add',
                button2: 'Edit',
                button3: 'Update',
                button4: 'Delete',
                show: 'Add',
                index: [0, 0],
                users: []
            },
            methods: {
                newPerson: function () {
                    let nameInput = this.nama.trim();
                    if (nameInput) {
                        this.$http.post('/api/user', {name: nameInput}).then(response => {
                            this.users.unshift(
                                {
                                    name: nameInput,
                                    id: response.body.data.id
                                }
                            )
                        })
                    }
                    this.nama = '';
                },
                changeButton: function (user, index) {
                    this.show = '';
                    this.nama = this.users[index]["name"];
                    this.index[0] = index;
                    this.index[1] = user.id;
                },
                updatePerson: function () {
                    let nameInput = this.nama.trim();
                    if (nameInput) {
                        this.$http.post('/api/user/change-name/' + this.index[1], {name: nameInput}).then(response => {
                            this.users[this.index[0]]["name"] = nameInput;
                        })
                    }
                    this.nama = '';
                    this.show = 'Add';
                },
                deletePerson: function (user, index) {
                    if (confirm("Are you sure?")) {
                        this.$http.post('/api/user/delete/' + user.id).then(response => {
                            this.users.splice(index, 1)
                        })
                    }
                }
            },
            mounted: function () {
                this.$http.get('/api/user').then(response => {
                    let result = response.body.data
                    this.users = result
                });
            }
        });
    </script>
</body>
</html>